from modes import *
import numpy as np
from math import pi, sqrt
from cmath import phase
import oct2py
from sys import argv
import re

def CoupledSeriesRLC(R1, L1, C1, R2, L2, C2, k):
    M = k * sqrt(L1 * L2)
    return (
        np.array([[L1, M], [M, L2]]),
        np.array([[R1, 0], [0, R2]]),
        np.array([[1.0/C1, 0], [0, 1.0/C2]]))

def CommonDifferentialCircuit(R, L, C, k):
    M = k * L
    return (
        np.array([
            [L,       -L,      M  ],
            [-L,      4*L-2*M, -L ],
            [M,       -L,      L  ]]),
        np.array([
            [R,       -R,       0  ],
            [-R,      4*R,     -R  ],
            [0,       -R,       R  ]]),
        np.array([
            [1/C,     0,       0  ],
            [0,       0,       0  ],
            [0,       0,       1/C]]))

def PrintModes(w, v):
    for i in range(len(w)):
        mode = v[:,i]
        mode /= max(mode, key=abs)
        mode /= np.linalg.norm(mode)
        sigma = w[i].real
        omega = w[i].imag
        if omega == 0:
            tau = -1.0 / sigma;
            print('t: {:.3f} /s'.format(tau))
        else:
            frequency = 2 * pi * omega
            q_factor = abs(omega / (2 * sigma))
            print('f: {:.3f} Hz'.format(frequency))
            print('Q: {:.3f}'.format(q_factor))
        for j in range(len(mode)):
            print('{}: {:.3f} @ {:.1f}\u00B0'.format(j, abs(mode[j]), 180*phase(mode[j])/pi))
        print()

params = {}
for arg in argv:
    match = re.match('^-(\w+)=(.+)$', arg)
    if match:
        params[match.group(1)]=float(match.group(2))
if '--common-differential' in argv:
    A, B, C = CommonDifferentialCircuit(*(params[l] for l in ('R', 'L', 'C', 'k')))
if '--coupled-rlc' in argv:
    A, B, C = CoupledSeriesRLC(*(params[l] for l in ('R1', 'L1', 'C1', 'R2', 'L2', 'C2', 'k')))
if '--oct2py' in argv:
    oc = oct2py.Oct2Py()
    w, v = oc.modes(A, B, C, nout=2)
    w = w[:,0]
else:
    w, v = modes(A, B, C)
PrintModes(w, v)