function [w, v] = modes(A, B, C)
    zero = zeros(size(A));
    identity = eye(size(A));
    [V, D] = eig([
        -C,       zero;
        zero,     identity ],[
        B,        A;
        identity, zero     ]);
    w = diag(D);
    v = V(1:size(A,1),1:end);
end