from scipy.linalg import eig
import numpy as np

def modes(A, B, C):
    zero = np.zeros(A.shape)
    identity = np.identity(A.shape[0])
    a = np.vstack((
            np.hstack( ( np.negative(C), zero     ) ),
            np.hstack( ( zero,           identity ) )
        ))
    b = np.vstack((
            np.hstack( ( B,              A        ) ),
            np.hstack( ( identity,       zero     ) )
        ))
    w, rv = eig(a, b)
    return (w, rv[:A.shape[0], :])