\documentclass[letterpaper,10pt]{article}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{indentfirst}
\usepackage[letterpaper, portrait, margin=0.5in]{geometry}
\usepackage{hyperref}
\usepackage{float}
\usepackage{gensymb}
\usepackage{minted}
\usemintedstyle{manni}
\title{Quadratic Eigenvalue Problem Example}
\author{Kyle Gagner}
\begin{document}
\maketitle

\begin{abstract}
In this experiment I investigate the modal analysis of two systems. The primary example is the modal analysis of two
series RLC circuits coupled by mutual inductance. Another system explored uses an arrangement of inductors which causes
two modes, a common mode where currents run together and a differential mode where they run opposed.
\end{abstract}

\section{Understanding the $s$-Domain}

\subsection{An Intuitive Approach}

An intuitive explanation begins with the assumption that the modes of the systems of interest are exponentially decaying
(or growing for unstable systems) sinusoids. Any real mode may be expressed as the linear combination of a complex
exponential and its conjugate, which simplifies the math. These complex exponentials are of the form $x(t)=x(0)e^{st}$.
With that assumption in mind, consider the equations of voltage and current for the basic linear circuit components:
resistors, capacitors, and inductors.
$$v_R(t)=Ri_R(t)$$
$$i_C(t)=C\frac{d}{dt}\left[v_C(t)\right]$$
$$v_L(t)=L\frac{d}{dt}\left[i_L(t)\right]$$
Now, substituting the complex exponential form, the derivatives can be simplified and the equations become monomial
terms of $s$.
$$v_R(0)e^{st}=Ri_R(0)e^{st}$$
$$i_C(0)e^{st}=sCv_C(0)e^{st}$$
$$v_L(0)e^{st}=sLi_L(0)e^{st}$$
Interestingly, complex impedances $v(t)=Zi(t)$ can be derived from here which express the impedance of each 
component.
$$Z_R=R$$
$$Z_C=\frac{1}{sC}$$
$$Z_L=sL$$

The remaining question is what $s$ really represents. To answer this, we break it into a real and imaginary part
$s=\sigma+j\omega$. Now $x(t)=x(0)e^{\sigma t}e^{j\omega t}$. $\sigma$ represents exponential decay (for negative
values, or exponential growth for positive values) and $\omega$ represents frequency. For complex impedances, it is
useful to explain the impedance in cartesian form $Z=R+jX$. $R$ is the resistance causing power to be dissipated in the
component. $X$ is the reactance causing current and voltage waveforms 90\degree out of phase so that the component
stores energy and expends stored energy but neither consumes nor produces any real power. While resistors are purely
resistive and capacitors and inductors are purely reactive, circuits combining these components may in general occupy
any part of the left hand side of the $s$-plane. The right hand side represents circuits with negative resistance which
cannot be achieved using passive components alone.

The choice of terms of $e^{st}$ is not a uniquely valid choice, but it is a particularly useful one. The challenge is
not in understanding why it's correct. Any set of functions which can be linearly combined to represent all continuous
functions of $t$ is equally valid. Rather, the challenge is in understanding the purpose of the $s$-domain as an
analytical tool.

\subsection{Using the Laplace Transform}
A more formal approach is to use the unilateral Laplace transform.
$$\mathcal{L}\left\{x(t)\right\}=\int_0^\infty x(t)e^{-st}dt=X(s)$$
Using the inductor as an example, the Laplace transform can also be used to derive the complex impedance.
$$\mathcal{L}\left\{v_L(t)\right\}=\mathcal{L}\left\{L\frac{d}{dt}\left[i_L(t)\right]\right\}$$
$$V(s)=\int_0^\infty \frac{d}{dt}\left[i_L(t)\right] e^{-st}dt$$
This can be solved using integration by parts.
$$u=e^{-st}$$
$$du=-se^{-st}dt$$
$$dv=\frac{d}{dt}\left[i_L(t)\right]dt$$
$$v=i_L(t)$$
$$V(s)=L\int_0^\infty udv$$
$$V(s)=\left[uv\right]_0^\infty-\int_0^\infty vdu$$
For bounded inputs ($\Re(s) < 0$), $uv$ will vanish to $0$ as $t\to\infty$.
$$V(s)=Li(0) + sL\int_0^\infty i(t)e^{-st}dt$$
$$V(s)=Li(0) + sLI(s)$$
When analyzing systems at steady state, the transient term may be dropped.
$$V(s)=sLI(s)$$
$$Z=sL$$

\section{Coupled Series RLC Circuits}

\subsection{Constructing a Quadratic Eigenvalue Problem}
The system of series RLC circuits coupled by mutual inductance is illustrated in figure \ref{fig1}.

\begin{figure}[H]
\includegraphics[width=\textwidth]{fig1.pdf}
\caption{The system of coupled RLC circuits investigated.}
\label{fig1}
\end{figure}
The mutual inductance $M$ can be expressed using coupling coefficient $k$. $M=k\sqrt{L_1L_2}$\footnote{see
\url{https://en.wikipedia.org/wiki/Inductance\#Coupling_coefficient} for information on the coupling coefficient and the
impedance parameters of coupled inductors}. The system is described using the mesh current method where the sum of
voltages around each current loop is $0$ due to Kirchhoff's voltage law. The Laplace transform is used to express the
equations in the $s$-domain.
$$ \frac{1}{sC_1}I_1 + R_1I_1 + sL_1I_1 + sMI_2 = 0 $$
$$ \frac{1}{sC_1}I_1 + R_1I_1 + sL_1I_1 + sMI_2 = 0 $$

Multiplying each equation by $s$ to cancel the term in the denominator and expressing the problem as a matrix yields a
matrix in terms of quadratic polynomials of $s$.
$$ \begin{bmatrix}
s^2L_1 + sR_1 + \frac{1}{C_1} & s^2M \\
s^2M & s^2L_2 + sR_2 + \frac{1}{C_2} \end{bmatrix}
\begin{bmatrix} I_1 \\ I_2 \end{bmatrix}
= A(s)v = 0 $$

A trivial solution occurs for any $s$ when $I_1=I_2=0$. Non-trivial solutions occur where $s=\lambda$ where $A(\lambda)$
is singular. Equivalently, $\det(A(\lambda))=0$. This is a quadratic eigenvalue problem.
$$ \det(A(\lambda)) =
(\lambda^2L_1 + \lambda R_1 + \frac{1}{C_1})(\lambda^2L_2 + \lambda R_2 + \frac{1}{C_2})-(\lambda^2M)^2 = 0 $$
$$ (\lambda^2L_1 + \lambda R_1 + \frac{1}{C_1})(\lambda^2L_2 + \lambda R_2 + \frac{1}{C_2})-\lambda^4L_1L_2 = 0 $$

Any eigenvalue $\lambda$ may be interpreted in cartesian form as $\sigma+j\omega$, where $\sigma$ descibes the dampening
of the mode and $\omega$ its frequency since $e^{st}=e^{\sigma t}e^{j\omega t}$. For these eigenvalues, any $v$
satisfying $A(\lambda)v=0$ describes the shape of the mode. In particular, a set of orthogonal basis vectors spanning
the nullspace of $A(\lambda)$ will yield an orthogonal set of modes.

\subsection{Expressing the Quadratic Eigenvalue Problem as a Generalized Eigenvalue Problem}

Although the quadratic eigenvalue problem could be solved directly, in practice there are more existing tools for a
related problem, the generalized eigenvalue problem. Generalized eigenvalue problems take the following form:
$$ A_gv = \lambda B_gv $$
The quadratic eigenvalue problem is of an altogether different form, which we will transform into a generalized
eigenvalue problem.
$$ \lambda^2A_qv + \lambda B_qv + C_qv = 0 $$
The first step is to introduce a variable $v' = \lambda v$ which gives a system of two equations.
$$ \lambda A_qv' + \lambda B_qv + C_qv = 0 $$
$$ v' = \lambda v $$
This problem can be put into matrix form - note each submatrix here has the same dimensions as $A_q$, $B_q$, or $C_q$.
$$ \lambda
\begin{bmatrix} B_q & A_q \\ I & 0 \end{bmatrix}
\begin{bmatrix} v \\ v' \end{bmatrix} =
\begin{bmatrix} -C_q & 0 \\ 0 & I \end{bmatrix}
\begin{bmatrix} v \\ v' \end{bmatrix} $$
This is a generalized eigenvalue problem. For example, here is the generalized eigenvalue problem form of the coupled 
series RLC circuits problem:
$$ \lambda
\begin{bmatrix} R_1 & 0 & L_1 & M \\ 0 & R_2 & M & L_2 \\ 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \end{bmatrix}
\begin{bmatrix} I_1 \\ I_2 \\ v'_1 \\ v'_2 \end{bmatrix} = \begin{bmatrix}
-\frac{1}{C_1} & 0 & 0 & 0 \\
0 & -\frac{1}{C_2} & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \end{bmatrix}
\begin{bmatrix} I_1 \\ I_2 \\ v'_1 \\ v'_2 \end{bmatrix} $$

Various software packages have numeric solvers for problems of this form. For example, here is a code listing for
Python which constructs the matrices for the generalized eigenvalue problem using the matrices for the quadratic
eigenvalue problem, then uses scipy.linalg.eig to solve the problem numerically.

\inputminted{python}{modes.py}

Here is an equivalent code listing for Matlab / Octave.

\inputminted{octave}{modes.m}

\subsection{Results}

The system was solved using $R_1=1\Omega$, $L_1=1mH$, $C_1.1=1\mu F$, $R_2=1\Omega$, $L_2=1mH$, $C_2=1\mu F$, and
$k=0.01$. The capacitor values were made slightly different so that two distinct modes may be observed where energy is
mainly stored in one loop and loosely coupled into the other. As the results show, these two modes occur at slightly
different frequencies.

For each mode, three results are given, the frequency, the Q-factor, and the relative amplitudes and phases (in degrees)
of the currents. Note that modes come in conjugate pairs.

\inputminted{text}{results1.txt}

\section{Common Mode / Differential Mode Circuit}

\subsection{Explaining the Modes}

The common mode / differential mode circuit in figure \ref{fig2} uses two pairs of mutually coupled inductors to create
interesting modes\footnote{see
\url{https://en.wikipedia.org/wiki/Polarity_(mutual_inductance)} for information on the dot convention.}.

\begin{figure}[H]
\includegraphics[width=\textwidth]{fig2.pdf}
\caption{The common mode / differential mode circuit. Note: not shown is a parasitic resistance in series with each
inductor, nor the coupling between the two pairs of inductors top / bottom and left / right.}
\label{fig2}
\end{figure}

The idea of this circuit is to observe two very different modes. One mode expected has alternating currents in $I_1$ and
$I_3$ 180\degree out of phase consistent with the coupling of the left / right pair of inductors. The other mode
consists of alternating current in the outer loop, which is modeled by a superposition of currents in $I_1$, $I_2$, and
$I_3$ all in phase.

\subsection{Results}

The system was solved with $R=1\Omega$, $L=1mH$, $C=1\mu F$, and $k=0.9$. The modal analysis shows exactly the two modes
expected, as well as two modes which do not oscillate. One of these represents energy stored mostly in the inductors,
with a relatively small amount of energy stored in the capacitors as well, rapidly dissipated in an exponential decay.
The last mode is more difficult to explain and may not have a good physical analogy.

For the modes which do not oscillate, the result gives the time constant $\tau$ rather
than the frequency and Q-factor as these are not applicable.

\inputminted{text}{results2.txt}

\end{document}
