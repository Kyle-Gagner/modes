modes.pdf: modes.tex fig1.pdf fig2.pdf results1.txt results2.txt
	pdflatex -interactions=nonstopmode -shell-escape -halt-on-error modes.tex
	pdflatex -interactions=nonstopmode -shell-escape -halt-on-error modes.tex
	pdflatex -interactions=nonstopmode -shell-escape -halt-on-error modes.tex

fig1.pdf: fig1.py
	python3 fig1.py

fig2.pdf: fig2.py
	python3 fig2.py

results1.txt: experiments.py Makefile
	python3 experiments.py --coupled-rlc -R1=1 -L1=1e-3 -C1=1.1e-6 -R2=1 -L2=1e-3 -C2=1e-6 -k=0.01 > results1.txt

results2.txt: experiments.py Makefile
	python3 experiments.py --common-differential -R=1 -L=1e-3 -C=1e-6 -k=0.9 > results2.txt

clean:
	rm -f *.aux *.out *.log *.pdf results1.txt results2.txt