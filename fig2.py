import matplotlib
matplotlib.use('Agg')
import schemdraw as schem 
import schemdraw.elements as e
import matplotlib.pyplot as plt
from math import sin, cos, pi
import numpy as np
d1 = schem.Drawing()
C1 = d1.add(e.CAP, d='up')
T1 = d1.add(e.LINE, d='right')
L1 = d1.add(e.INDUCTOR2, d='down')
T2 = d1.add(e.LINE, to=C1.start)
L3 = d1.add(e.INDUCTOR2, d='right', xy=L1.start)
T3 = d1.add(e.LINE, d='right')
C2 = d1.add(e.CAP, d='down')
L4 = d1.add(e.INDUCTOR2, xy=L1.end, d='right')
L2 = d1.add(e.INDUCTOR2, d='up')
T4 = d1.add(e.LINE, xy=L4.end, to=C2.end)
d1.add(e.DOT, xy=[L1.start[0], L1.start[1]-0.5])
d1.add(e.DOT, xy=[L2.end[0], L2.end[1]-0.5])
d1.add(e.DOT, xy=[L3.start[0]+0.5, L3.start[1]])
d1.add(e.DOT, xy=[L4.start[0]+0.5, L4.start[1]])
d1.loopI([T1, L1, T2, C1], d='cw', label='$I_1$')
d1.loopI([L3, L2, L4, L1], d='cw', label='$I_2$')
d1.loopI([T3, C2, T4, L2], d='cw', label='$I_3$')
d1.draw()
d1.save('fig2.pdf')