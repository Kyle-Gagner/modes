import numpy as np
import itertools
from modes import modes
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d.axes3d import Axes3D
from cmath import exp
import matplotlib.animation as animation
from sys import argv

def adjacent(shape, i):
    row = i // shape[1]
    col = i % shape[1]
    if row > 0:
        yield i - shape[1]
    if row < shape[0] - 1:
        yield i + shape[1]
    if col > 0:
        yield i - 1
    if col < shape[1] - 1:
        yield i + 1

def pool(shape, c, v):
    count = shape[0] * shape[1]
    A = np.zeros((count, count))
    B = np.zeros_like(A)
    C = np.zeros_like(A)
    for i in range(count):
        A[i, i] = c
        B[i, i] = v
        for j in adjacent(shape, i):
            C[i, i] += 1
            C[i, j] = -1
    return A, B, C

def animate(t, ax, shape, eig, mode):
    bars = [(ind, loc[0], loc[1]) for ind, loc in enumerate(itertools.product(range(shape[0]), range(shape[1])))]
    ax.clear()
    v = exp(eig*t)
    ax.bar3d([x[1] for x in bars], [x[2] for x in bars], 0, 1, 1, 2+(v*mode).real)
    ax.set_zlim(bottom = 0, top = 4)

shape = (int(argv[1]), int(argv[2]))
A, B, C = pool(shape, 1, 0.1)
w, v = modes(A, B, C)
wave = int(argv[3])
eig = w[wave]
mode = v[:,wave]
mode /= max(abs(mode))

fig = plt.figure()
ax = Axes3D(fig)
framerate = 10
ani = animation.FuncAnimation(fig, animate,
    frames=itertools.count(0, 1.0/framerate),
    fargs=(ax, shape, eig, mode),
    interval=1e3/framerate)

plt.show()