import matplotlib
matplotlib.use('Agg')
import schemdraw as schem 
import schemdraw.elements as e
import schemdraw.segments as s
import matplotlib.pyplot as plt
from math import sin, cos, pi
import numpy as np

d1 = schem.Drawing()
C1 = d1.add(e.CAP, d='up', rgtlabel='$C_1$')
R1 = d1.add(e.RES, d='right', toplabel='$R_1$')
L1 = d1.add(e.INDUCTOR2, d='down', rgtlabel='$L_1$')
T1 = d1.add(e.LINE, to=C1.start)
d1.loopI([R1, L1, T1, C1], d='cw', label='$I_1$')
L2 = d1.add(e.INDUCTOR2, xy=[L1.end[0]+d1.unit, L1.end[1]], d='up', rgtlabel='$L_2$')
R2 = d1.add(e.RES, d='right', toplabel='$R_1$')
C2 = d1.add(e.CAP, d='down', rgtlabel='$C_2$')
T2 = d1.add(e.LINE, to=L2.start)
d1.loopI([R2, C2, T2, L2], d='cw', label='$I_2$')
theta = 20.0

class Mutual(e.Element):
    def __init__(self, *args, **kwargs):
        e.Element.__init__(self, *args, **kwargs)
        self.segments.append(s.SegmentArc(
            [d1.unit/2, -d1.unit*cos(theta*pi/180.0)],
            d1.unit,
            d1.unit,
            theta1 = 90-theta,
            theta2 = 90+theta,
            arrow = 'ccw'))
        self.segments.append(s.SegmentArc(
            [d1.unit/2, -d1.unit*cos(theta*pi/180.0)],
            d1.unit,
            d1.unit,
            theta1 = 90-theta,
            theta2 = 90+theta,
            arrow = 'cw'))

d1.add(Mutual, xy=L1.start, d='right', toplabel='M')
d1.draw()
d1.save('fig1.pdf')