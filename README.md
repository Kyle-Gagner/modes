# Modes

An experiment in modal analysis

## Required Tools

- pdflatex
  - minted
- Python 3
  - scipy
  - numpy
  - matplotlib
  - SchemDraw
  - oct2py
- GNU Octave

## Commands

To build the report:
```bash
make
```

To run the pool animated graph:
```bash
python3 pool.py <width> <depth> <mode number>
```

To run the coupled series RLC experiments:
```bash
python3 experiments.py --coupled-rlc [--oct2py] -R1=<R1> -L1=<L1> -C1=<C1> -R2=<R2> -L2=<L2> -C2=<C2> -k=<k>
```

To run the common / differential mode experiments:
```bash
python3 experiments.py --common-differential [--oct2py] -R=<R> -L=<L> -C=<C> -k=<k>
```